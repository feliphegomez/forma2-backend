# forma2backend

## Forma2

1. Ingresar a la carpeta .virtualenvs.
~~~sh
    $ cd ~/.virtualenvs
~~~

2. Crear el entorno virtual..

~~~sh
    $ virtualenv forma2-back -p /usr/bin/python3.5
~~~


~~~sh
	$ python -m virtualenv forma2-back
~~~

3. Crear la llave en servidor.
~~~sh
    $ ssh-keygen
~~~

4. Copiar la la informacion que se encuentra dentro del archivo 
	[id_rsa.pub] que se encuentra en la carpeta [.ssh] del ususario-
~~~sh
	$ cd ~/.ssh/
	$ vim id_rsa.pub
~~~

5. Guardar la clave en BITBUCKET para que se autorice la clonacion del repositorio y las futuras modificaciones.

6. Ir a la carpeta donde se va a almacenar todo el proyecto.
~~~sh	
    $ cd ~/projects/
~~~

7. Clonar el repositorio en la carpeta projects.
~~~sh
	$ git clone git@bitbucket.org:feliphegomez/forma2-backend.git
~~~

8. Activamos el entorno virtual.
~~~sh
	$ source ~/.virtualenvs/forma2-back/bin/activate
~~~

9. Instalar django, djangorestframework, pygments, django-cors-headers
~~~ sh
	$ pip install django
	$ pip install django-cors-headers
	$ pip install Pillow
	$ pip install pygments  # Usaremos esto para resaltar el codigo.
	$ pip install markdown  # Usaremos esto los archivos .md
	$ pip install django-filter
	$ pip install django-filters
	$ pip install django-rest-auth
	$ pip install djangorestframework
	$ pip install djangorestframework-jwt
	$ pip install django-rest-auth
	$ pip install requests
	$ pip install django-debug-toolbar
	$ pip install coreapi
~~~

10. instalar mysql client en el servidor (Omitir si ya se tiene instalado)
~~~sh
	$ sudo apt-get update
	$ sudo apt-get install mysql-server
	$ sudo mysql_secure_installation
	$ sudo apte-get install mysql-client
~~~

11. Configurar root remoto global (omitir en caso de ya estar configurado)
	a. INGRESAR A LA BD DE MYSQL:
~~~sh
	$ mysql -h localhost -u root -p
	$ mysql> CREATE USER 'forma2'@`%` IDENTIFIED BY 'Forma2.V1';
	$ mysql> SET GLOBAL validate_password_policy = LOW;
	$ mysql> GRANT ALL PRIVILEGES ON *.* TO 'forma2'@'%' WITH GRANT OPTION;
	$ mysql> CREATE DATABASE forma2_db;	
	$ mysql> FLUSH PRIVILEGES;
	$ mysql> quit
~~~

12. Configurar root remoto global 2da parte:
~~~sh
	$ sudo vim /etc/mysql/my.cnf
	"[mysqld]
	  bind-address = 0.0.0.0"
~~~

13. Reiniciar MySQL
~~~sh 
	$ sudo service mysql restart
~~~

14. instalar python-dev, pymysql
~~~sh
	$ sudo apt-get install python-dev
	$ pip install pymysql
~~~

15. Ir al archivo __init__.py donde se aloja el archivo settings.py  y agregue el siguiente codigo o eliminelo para utilizar SQLite
~~~sh
	import pymysql
	pymysql.install_as_MySQLdb()
~~~

16. Migrar Base de datos
~~~sh
	$ python forma2back/manage.py migrate
~~~

17. Ejecutar el servidor
