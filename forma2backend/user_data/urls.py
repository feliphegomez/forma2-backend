'''
Urls de application
'''

from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from user_data.views import *


#from user_data.views import *
router = DefaultRouter()
# Vistas provenientes de user_data -> Emtelco
#router.register(r'permissions', PermissionViewSet, base_name='permissions')
#router.register(r'groups', GroupViewSet, base_name='groups')
#router.register(r'users', UserViewSet, base_name='users')
#router.register(r'users-full', UserFullViewSet, base_name='users-full')


urlpatterns = [
    url(r'^users/', include(router.urls)),
]
