# Generated by Django 2.0.6 on 2018-07-23 15:53

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('app', '0010_auto_20180723_0955'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('document', models.CharField(max_length=11, validators=[django.core.validators.RegexValidator('^[0-9]*$', code='Inválido', message='Este campo debe ser numérico.')], verbose_name='Documento')),
                ('login_type', models.CharField(choices=[('LDAP', 'Directorio activo'), ('APPLICATION', 'Aplicación')], max_length=20, verbose_name='Tipo de login')),
                ('boss', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_data_boss', to=settings.AUTH_USER_MODEL)),
                ('pilots', models.ManyToManyField(blank=True, to='app.Pilot')),
                ('roles', models.ManyToManyField(blank=True, to='app.Role')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='user_data_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Datos de usuario',
                'default_permissions': (),
                'verbose_name_plural': 'Datos de usuarios',
                'permissions': (('add_userdata', 'Crear usuarios'), ('change_userdata', 'Actualizar un usuario'), ('list_userdata', 'Consultar usuarios'), ('retrieve_userdata', 'Consultar un usuario'), ('add_groups', 'Crear un grupo'), ('change_groups', 'Actualizar un grupo'), ('list_groups', 'Consultar grupos'), ('retrieve_groups', 'Consultar un grupo')),
            },
        ),
        migrations.CreateModel(
            name='UserDataLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('field', models.CharField(max_length=150, null=True, verbose_name='Campo')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Fecha creación')),
                ('deleted', models.BooleanField(default=False, verbose_name='Eliminar')),
                ('before_char', models.CharField(blank=True, max_length=500, null=True, verbose_name='Texto antes')),
                ('after_char', models.CharField(blank=True, max_length=500, null=True, verbose_name='Texto después')),
                ('before_text', models.TextField(blank=True, null=True, verbose_name='Texto largo antes')),
                ('after_text', models.TextField(blank=True, null=True, verbose_name='Texto largo después')),
                ('before_id', models.IntegerField(blank=True, null=True, verbose_name='Id antes')),
                ('after_id', models.IntegerField(blank=True, null=True, verbose_name='Id después')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_data_userdatalog_created_by', to=settings.AUTH_USER_MODEL, verbose_name='Creado por')),
                ('record', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='user_data.UserData', verbose_name='Registro')),
            ],
            options={
                'default_permissions': (),
                'permissions': (('retrieve_user_datalog', 'Consultar log de un usuario'), ('list_user_datalog', 'Consultar log de usuarios')),
            },
        ),
    ]
