'''
Vista de usuarios
'''

from django_filters import rest_framework as filters
from src.view import BaseViewSet
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.decorators import detail_route, action
from rest_framework.response import Response
from django.contrib.auth.models import User, Group, Permission
from django.db.models import Q
from user_data.serializers.user import UserSerializer, UserFullSerializer
from user_data.serializers.group import GroupSerializer
from user_data.serializers.permission import PermissionSerializer
from user_data.serializers.create_user import CreateUserSerializer
from user_data.filters.user import UserFilter


class UserViewSet(BaseViewSet):
    '''
    ViewSet para usuarios
    '''

    app_code = 'user_data'
    permission_code = 'userdata'
    queryset = User.objects.all().order_by('-id').prefetch_related('groups').select_related('user_data_user')
    serializer_class = UserSerializer
    filter_class = UserFilter
    search_fields = ('username', 'first_name', 'last_name', 'email', 'user_data_user__document')
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    @action(detail=False)
    def managers(self, request):
        '''
        Método para realizar la consulta de todos los Gestores del aplicativo.
        '''
        permission_code = None
        managers = User.objects.filter(is_active=1, groups__name__in=['Gestor', 'Supervisor']).order_by('username').distinct()
        user_serializer = UserSerializer(managers, many=True)
        
        return Response(user_serializer.data)
    
    @action(detail=False)
    def bosses(self, request):
        '''
        Método para realizar la consulta de todos Supervisores y Ejecutivos para la asignación de jefes.
        '''
        permission_code = None
        managers = User.objects.filter(Q(is_active=1, groups__name='Supervisor') | Q(is_active=1, groups__name='Ejecutivo')).distinct().order_by('username')
        user_serializer = UserSerializer(managers, many=True)
        
        return Response(user_serializer.data)


class UserFullViewSet(BaseViewSet):
    '''
    ViewSet para usuarios
    '''

    app_code = 'user_data'
    permission_code = 'userdata'
    queryset = User.objects.all().order_by('-id').prefetch_related('groups').select_related('user_data_user')
    serializer_class = UserFullSerializer
    filter_class = UserFilter
    search_fields = ('username', 'first_name', 'last_name', 'email', 'user_data_user__document')
    ordering_fields = ('id', 'username', 'first_name', 'last_name', 'email', 'user_data_user__document')
