'''
    PermissionViewSet by Juan David González Bedoya
'''

from django.contrib.auth.models import Permission
from rest_framework.response import Response

from src.view import BaseViewSet

from user_data.serializers.permission import PermissionSerializer
from user_data.filters.permission import PermissionFilter


class PermissionViewSet(BaseViewSet):
    '''
    VieSet para permisos
    '''

    app_code = 'user_data'
    permission_code = 'permission'
    queryset = Permission.objects.all().order_by('-id').exclude(content_type__in=[1,2,3,4,5,6])
    serializer_class = PermissionSerializer
    filter_class = PermissionFilter
    search_fields = ('name', 'codename')
    ordering_fields = ('name', 'codename')

    def list(self, request):
        self.permission_code = None
        return super().list(request)

    def retrieve(self, request, pk=None):
        self.permission_code = None
        return super().retrieve(request, pk)

    def create(self, request):
        return Response({'detail': 'Acceso inválido'})

    def update(self, request, pk=None):
        return Response({'detail': 'Acceso inválido'})

    def partial_update(self, request, pk=None):
        return Response({'detail': 'Acceso inválido'})

    def destroy(self, request, pk=None):
        return Response({'detail': 'Acceso inválido'})
