"""
Definición __init__
"""
from .user import UserViewSet, UserFullViewSet
from .group import GroupViewSet
from .permission import PermissionViewSet
