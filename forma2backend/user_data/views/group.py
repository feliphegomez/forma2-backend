'''
    GroupViewSet
'''
from src.view import BaseViewSet
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from django.contrib.auth.models import Group, Permission
from user_data.serializers.group import GroupSerializer
from user_data.serializers.permission import PermissionSerializer
from user_data.filters.group import GroupFilter


class GroupViewSet(BaseViewSet):
    '''
    Vista para Roles
    '''

    app_code = 'user_data'
    permission_code = 'groups'
    queryset = Group.objects.all().order_by('-id')
    serializer_class = GroupSerializer
    filter_class = GroupFilter
    search_fields = ('name', 'permissions__codename')
    ordering_fields = ('name',)

    def destroy(self, request, pk=None):
        group = self.get_object()
        if group.user_set.all() or group.permissions.all():
            return Response({'detail': 'El grupo tiene usuarios y/o permisos asignados por ende no puede ser eliminado.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return super().destroy(request, pk)

    @detail_route(methods=['GET', 'POST'])
    def permissions(self, request, pk=None):
        '''
        Asignación de permisos
        '''
        group = self.get_object()
        if request.method=='GET':
            serializer_permissions = PermissionSerializer(group.permissions.all(), many=True)
            return Response(serializer_permissions.data)
        else:
            id_permissions = request.data['permissions']
            permissions=[]
            for id_permission in id_permissions:
                try:
                    permission = Permission.objects.get(pk=id_permission)
                    permissions.append(permission)
                except Exception:
                    return Response({'detail': 'El permiso con ID "{0}" no existe. Por favor valide los datos.'.format(id_permission)}, status=status.HTTP_406_NOT_ACCEPTABLE)
            group.permissions.set(permissions)
            serializer_permissions = PermissionSerializer(group.permissions.all(), many=True)
            return Response(serializer_permissions.data)
