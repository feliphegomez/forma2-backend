'''
Modelo de user_data
'''
from django.db import models
from django.contrib.auth.models import User
from lib.validators import isnumbervalidator
from src.model import BaseLogModel
from app.models.pilot import Pilot


LOGIN_LDAP = 'LDAP'
LOGIN_APPLICATION = 'APPLICATION'

LOGIN_TYPE_LIST = [
    (LOGIN_APPLICATION, 'Aplicación'),
    (LOGIN_LDAP, 'Directorio activo'),
]


class UserDataLog(BaseLogModel):
    '''Log modelo de user_data'''
    record = models.ForeignKey('UserData', verbose_name='Registro', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_user_datalog', 'Consultar log de un usuario'),
                       ('list_user_datalog', 'Consultar log de usuarios'))


class UserData(models.Model):
    '''
    Data para el usuario
    '''

    log_class = UserDataLog

    user = models.OneToOneField(User, related_name='user_data_user', on_delete=models.CASCADE)
    boss = models.ForeignKey(User, related_name='user_data_boss', null=True, on_delete=models.CASCADE)
    document = models.CharField('Documento', max_length=11, validators=[isnumbervalidator])
    login_type = models.CharField('Tipo de login', choices=LOGIN_TYPE_LIST, max_length=20)
    # pilots = models.ManyToManyField(Pilot, blank=True)
    # pilot = models.OneToOneField(Pilot, related_name='user_data_pilot', on_delete=models.PROTECT, default=False)

    def __str__(self):
        '''
        Retornar por default el first_name
        '''
        return self.user.first_name

    class Meta:
        '''
        Meta tags
        '''
        verbose_name_plural = 'Datos de usuarios'
        verbose_name = 'Datos de usuario'
        default_permissions = ()
        permissions = (
            ('add_userdata', 'Crear usuarios'),
            ('change_userdata', 'Actualizar un usuario'),
            ('list_userdata', 'Consultar usuarios'),
            ('retrieve_userdata', 'Consultar un usuario'),
            ('add_groups', 'Crear un grupo'),
            ('change_groups', 'Actualizar un grupo'),
            ('list_groups', 'Consultar grupos'),
            ('retrieve_groups', 'Consultar un grupo')
        )
