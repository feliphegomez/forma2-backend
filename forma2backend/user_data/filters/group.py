'''
    group
'''
from django_filters import rest_framework as filters
from django.contrib.auth.models import Group


class GroupFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = Group
        fields = {
            'name': ['contains', 'exact'],
        }
