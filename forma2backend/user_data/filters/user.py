'''
    user
'''
from django_filters import rest_framework as filters
from django.contrib.auth.models import User


class UserFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = User
        fields = {
            'id': ['in'],
            'first_name': ['icontains', 'exact'],
            'last_name': ['icontains', 'exact'],
            'username': ['icontains', 'exact'],
            'email': ['icontains', 'exact'],
            'is_active': ['exact'],
            'user_data_user__document': ['icontains', 'exact'],
            'user_data_user__login_type': ['exact'],
            'user_data_user__boss': ['exact'],
            'groups': ['exact']
        }
