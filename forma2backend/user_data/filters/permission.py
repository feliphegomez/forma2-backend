'''
    permission
'''
from django_filters import rest_framework as filters
from django.contrib.auth.models import Permission


class PermissionFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = Permission
        fields = {
            'name': ['contains'],
            'codename': ['contains'],
        }
