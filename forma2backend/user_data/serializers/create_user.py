import string
import random
import requests
from rest_framework import serializers
from django.contrib.auth.models import User
from django.conf import settings
from user_data.models import LOGIN_LDAP, LOGIN_APPLICATION, LOGIN_TYPE_LIST, UserData
from user_data.serializers.user import UserSerializer
from user_data.serializers.userdata import UserDataSerializer
from lib.validators import isnumbervalidator


class CreateUserSerializer(serializers.Serializer):
    username = serializers.CharField(label='Nombre de usuario', required=True)
    email = serializers.EmailField(label='Correo electrónico', required=False)
    first_name = serializers.CharField(label='Nombre(s)', required=False)
    last_name = serializers.CharField(label='Apellido(s)', required=False)
    is_active = serializers.BooleanField(label='Activo', required=False, default=True)
    is_superuser = serializers.BooleanField(label='Super usuario', required=False)
    document = serializers.CharField(label='Documento de identidad', required=False, validators=[isnumbervalidator])
    login_type = serializers.ChoiceField(label='Tipo de login', choices=LOGIN_TYPE_LIST, required=True)
    password = serializers.CharField(label='Contraseña', required=True, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirme contraseña', required=True, style={'input_type': 'password'}, write_only=True)

    def __init__(self, *args, **kwargs):
        # No pasar "fields" a la super clase
        fields = kwargs.pop('fields', None)

        # Instanciar la clase super
        super(CreateUserSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Eliminar cualquier campo que no esté en el argumento fields
            allowed = set(fields)
            existing = set(self.fields)
            for field_name in existing - allowed:
                self.fields.pop(field_name)
        
        try:
            if self.initial_data['login_type'] == LOGIN_APPLICATION:
                self.fields['email'].required = True
                self.fields['first_name'].required = True
                self.fields['last_name'].required = True
                self.fields['document'].required = True
            else:
                self.fields['password'].required = False
                self.fields['confirm_password'].required = False
        except:
            pass


    def validate_username(self, value):
        existing = User.objects.filter(username=value).first()
        if existing:
            raise serializers.ValidationError('El usuario ya existe.')
        else:
            if self.initial_data['login_type'] == LOGIN_APPLICATION:
                try:
                    ws_reponse = requests.post(
                        settings.PYPERSONAL_URL,
                        {
                            'user': self.initial_data['username'],
                            'fields': '["first_name"]'
                        }
                    )

                    if ws_reponse.status_code == requests.codes['ok']:
                        raise serializers.ValidationError('El usuario ya existe en el directorio activo, no puede ser registrado por \"Aplicación\".')
                except Exception as e:
                    raise serializers.ValidationError(e.detail)
        return value


    def validate_confirm_password(self, value):
        if self.initial_data['password'] != value and self.initial_data['login_type'] == LOGIN_APPLICATION:
            raise serializers.ValidationError('Las contraseñas no coinciden.')
        
        return value

    
    def validate(self, data):
        if data['login_type'] == LOGIN_LDAP:
            try:
                ws_reponse = requests.post(
                    settings.PYPERSONAL_URL,
                    {
                        'user': data['username'],
                        'fields': '["first_name", "last_name", "mail", "document"]'
                    }
                )

                content = ws_reponse.json()

                if ws_reponse.status_code == requests.codes['ok']:
                    data['first_name'] = content['epersonal']['first_name']
                    data['last_name'] = content['epersonal']['last_name']
                    data['email'] = content['ldap']['mail']
                    data['document'] = content['ldap']['document']
                    data['login_type'] = LOGIN_LDAP
                    data['password'] = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))
                elif ws_reponse.status_code == requests.codes['not_found']:
                    raise serializers.ValidationError(content['user'])
                elif ws_reponse.status_code == requests.codes['forbidden']:
                    raise serializers.ValidationError(content['general'])
                elif ws_reponse.status_code == requests.codes['internal_server_error']:
                    raise serializers.ValidationError(content['general'])
                else:
                    raise serializers.ValidationError('Ocurrió un error al recolectar datos del directorio activo.')
            except Exception as e:
                raise serializers.ValidationError(e.detail)
        
        return data

    def save(self):
        data = self.validated_data.copy()
        try:
            data.pop('confirm_password')
        except:
            pass
        user_data = {
            'document': data.pop('document'),
            'login_type': data.pop('login_type'),
            # 'pilot': data.pop('pilot'),
        }

        UserData.objects.create(user=User.objects.create_user(**data), **user_data)
