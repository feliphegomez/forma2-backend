'''
serializadores de datos de usuario
'''
from rest_framework import serializers
from user_data.models import UserData


class UserDataSerializer(serializers.ModelSerializer):
    '''
    UserData serializer
    '''

    class Meta:
        '''
        Meta tags
        '''
        model = UserData
        fields = (
            'document',
            'login_type',
            'user_id',
            'boss',
            # 'pilot',
        )


class UserDataFullSerializer(serializers.ModelSerializer):
    '''
    UserData full serializer
    '''

    login_type = serializers.CharField(source='get_login_type_display')

    class Meta:
        '''
        Meta tags
        '''
        model = UserData
        fields = (
            'document',
            'login_type',
            'user_id',
            'boss',
            # 'pilot',
        )
