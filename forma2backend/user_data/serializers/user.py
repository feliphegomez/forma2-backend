'''
    UserSerializer by Juan David González Bedoya
'''
import string
import random
import requests
from django.conf import settings
from django.contrib.auth.models import User, Group

from django.db.models import Q
from rest_framework import serializers

from user_data.models import UserData, LOGIN_APPLICATION, LOGIN_LDAP, LOGIN_TYPE_LIST
from user_data.serializers.userdata import UserDataSerializer, UserDataFullSerializer
from lib.validators import isnumbervalidator

from app.models.pilot import Pilot
from app.serializers.pilot_serializer import PilotSerializer

BOOS_LIST = User.objects.filter(Q(is_active=1, groups__name='Supervisor') | Q(is_active=1, groups__name='Ejecutivo')).distinct()
PILOTS_LIST = Pilot.objects.filter().distinct()


class UserSerializer(serializers.ModelSerializer):
    '''
    User serializer
    '''

    user_data_user = UserDataSerializer(many=False, read_only=True)
    email = serializers.EmailField(label='Correo electrónico', required=False, allow_null=True, allow_blank=True)
    first_name = serializers.CharField(label='Nombre(s)', required=False, allow_null=True, allow_blank=True)
    last_name = serializers.CharField(label='Apellido(s)', required=False, allow_null=True, allow_blank=True)
    password = serializers.CharField(label='Contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirme contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    document = serializers.CharField(label='Documento de identidad', required=False, allow_null=True, allow_blank=True, write_only=True, validators=[isnumbervalidator])
    login_type = serializers.ChoiceField(label='Tipo de login', choices=LOGIN_TYPE_LIST, required=True, write_only=True)
    boss = serializers.ChoiceField(label='Jefe', choices=BOOS_LIST, allow_blank=False, allow_null=True, required=True, write_only=True)
    
    # pilot = serializers.ChoiceField(label='Piloto', choices=PILOTS_LIST, allow_blank=False, allow_null=True, required=True, write_only=True)
    # pilots = PilotSerializer(many=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'data' in kwargs:
            if 'partial' in kwargs:
                self.fields['first_name'].required = False
                self.fields['first_name'].allow_blank = True
                self.fields['last_name'].required = False
                self.fields['last_name'].allow_blank = True
                self.fields['email'].required = False
                self.fields['email'].allow_blank = True
                self.fields['password'].required = False
                self.fields['password'].allow_blank = True
                self.fields['confirm_password'].required = False
                self.fields['confirm_password'].allow_blank = True
                self.fields['document'].required = False
                self.fields['document'].allow_blank = True
                # self.fields['pilot'].required = True
                # self.fields['pilot'].allow_null = False
                # self.fields['pilot'].allow_blank = False
            elif kwargs['data']['login_type'] == LOGIN_APPLICATION:
                self.fields['first_name'].required = True
                self.fields['first_name'].allow_null = False
                self.fields['first_name'].allow_blank = False
                self.fields['last_name'].required = True
                self.fields['last_name'].allow_null = False
                self.fields['last_name'].allow_blank = False
                self.fields['email'].required = True
                self.fields['email'].allow_null = False
                self.fields['email'].allow_blank = False
                self.fields['password'].required = True
                self.fields['password'].allow_null = False
                self.fields['password'].allow_blank = False
                self.fields['confirm_password'].required = True
                self.fields['confirm_password'].allow_null = False
                self.fields['confirm_password'].allow_blank = False
                self.fields['document'].required = True
                self.fields['document'].allow_null = False
                self.fields['document'].allow_blank = False
                # self.fields['pilot'].required = True
                # self.fields['pilot'].allow_null = False
                # self.fields['pilot'].allow_blank = False
            

    def validate(self, data):
        if data['login_type'] == LOGIN_LDAP:
            try:
                ws_reponse = requests.post(
                    settings.PYPERSONAL_URL,
                    {
                        'user': data['username'],
                        'fields': '["first_name", "last_name", "mail", "document"]'
                    }
                )
                
                content = ws_reponse.json()
                if ws_reponse.status_code == requests.codes['ok']:
                    data['first_name'] = content['epersonal']['first_name']
                    data['last_name'] = content['epersonal']['last_name']
                    data['email'] = content['ldap']['mail']
                    data['document'] = content['ldap']['document']
                elif ws_reponse.status_code == requests.codes['not_found']:
                    raise serializers.ValidationError(content['user'])
                elif ws_reponse.status_code == requests.codes['forbidden']:
                    raise serializers.ValidationError(content['general'])
                elif ws_reponse.status_code == requests.codes['internal_server_error']:
                    raise serializers.ValidationError(content['general'])
                else:
                    raise serializers.ValidationError('Ocurrió un error al recolectar datos del directorio activo.')
            except Exception as e:
                raise serializers.ValidationError(e.detail)
        
        return data


    def update(self, instance, validated_data):
        userdata = instance.user_data_user

        instance.username = validated_data.get('username', instance.username)
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.is_superuser = validated_data.get('is_superuser', instance.is_superuser)

        instance.groups.set(validated_data['groups'])
        
        instance.save()
        
        userdata.document = validated_data['document'] if validated_data['document'] else userdata.document
        userdata.login_type = validated_data.get('login_type', userdata.login_type)

        # userdata.pilot = validated_data.get('pilot', userdata.pilot.set())

        if validated_data['boss'] == instance:
            raise serializers.ValidationError('El usuario no puede ser jefe de si mismo.')
        else:
            userdata.boss = validated_data.get('boss', userdata.boss)
        userdata.save()

        return instance


    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        try:
            validated_data.pop('confirm_password')
        except:
            pass

        user_data = {
            'document': validated_data.pop('document'),
            'boss': validated_data.pop('boss'),
            'login_type': validated_data.pop('login_type'),
            # 'pilot': validated_data.pop('pilot'),
        }
        user = User.objects.create_user(**validated_data)
        UserData.objects.create(user=user, **user_data)
        user.groups.set(groups_data)
        return user

    class Meta:
        '''
        Meta tags
        '''
        model = User
        fields = (
            'id',
            'username',
            'login_type',
            'boss',
            'email',
            'first_name',
            'last_name',
            'is_active',
            'is_superuser',
            'user_data_user',
            'groups',
            'password',
            'confirm_password',
            'document',
            # 'pilot',
        )


class UserFullSerializer(serializers.ModelSerializer):
    '''
    User serializer
    '''

    user_data_user = UserDataFullSerializer(many=False, read_only=True)
    document = serializers.CharField(label='Documento de identidad', read_only=True)

    class Meta:
        '''
        Meta tags
        '''
        model = User
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'is_active',
            'is_superuser',
            'user_data_user',
            'document',
            'groups',
        )