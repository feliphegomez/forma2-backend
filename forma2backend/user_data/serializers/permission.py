'''
Serializadores de permisos
'''
from rest_framework import serializers
from django.contrib.auth.models import Permission


class PermissionSerializer(serializers.ModelSerializer):
    '''
    Serializador de permisos
    '''
    class Meta:
        '''
        Meta tags
        '''
        model = Permission
        fields = ('id', 'name', 'content_type', 'codename')
'''
Serializadores de permisos
'''
from rest_framework import serializers
from django.contrib.auth.models import Permission


class PermissionSerializer(serializers.ModelSerializer):
    '''
    Serializador de permisos
    '''
    class Meta:
        '''
        Meta tags
        '''
        model = Permission
        fields = ('id', 'name', 'content_type', 'codename')
