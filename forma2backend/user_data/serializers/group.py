'''
Serializadores de grupos
'''
from rest_framework import serializers
from django.contrib.auth.models import Group


class GroupSerializer(serializers.ModelSerializer):
    '''
    Serializador de Grupos
    '''

    class Meta:
        '''
        Meta tags
        '''

        model = Group
        fields = (
            'id',
            'name',
            'permissions',
            'user_set'
        )
