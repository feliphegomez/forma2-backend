"""
Definición __init__
"""
from user_data.serializers.user import UserSerializer, UserFullSerializer
from user_data.serializers.userdata import UserDataSerializer, UserDataFullSerializer
from user_data.serializers.group import GroupSerializer
from user_data.serializers.permission import PermissionSerializer
from user_data.serializers.create_user import CreateUserSerializer
