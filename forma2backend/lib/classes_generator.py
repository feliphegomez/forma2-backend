
from django.db import models
from rest_framework import serializers
from django_filters import rest_framework as filters

def get_model(name_model, label_table, fields):
    '''
    Creación de modelo dinamico usando un type para crear una clase manualmente.
    '''

    # Creamos una clase meta y posteriormente le agregamos atributos
    class Meta:
        pass

    structure = {}
    # Se le agregan atributos a la clase meta creada anteriormente
    setattr(Meta, 'app_label', label_table)
    attrs = {
        '__module__': '',
        'Meta': Meta,
    }
    # Por cada uno de los campos de la estructura de la campaña se crea un charfield estandar para ser usado por el ORM
    for field in fields:
        if field == 'created_at':
            structure[field] = models.DateField(auto_now=True)
        else:
            structure[field] = models.CharField(max_length=255, blank=True, null=True)
    # cargamos la estructura en los attrs
    attrs.update(structure)

    # Creamos la clase del modelo haciendo uso de la función de python TYPE y le pasamos los attrs creados
    model = type(name_model, (models.Model,), attrs)

    return model


def get_serializer(model, fields):
    '''
    Creación de serialiador dinamico el cual recibe una clase modelo y unos fields para poder
    '''

    # Creamos una clase meta y posteriormente le agregamos atributos
    class Meta:
        pass

    # Le agregamos a la clase meta el model y los fields
    setattr(Meta, 'model', model)
    setattr(Meta, 'fields', fields)
    attrs = {
        'info_data': {
            'order': {},
            'fields': {}
        },
        'Meta': Meta,
    }

    # Se usa la función TYPE de python para crear el serializador haciendo uso de los attrs creados
    serializer = type('model', (serializers.ModelSerializer,), attrs)

    return serializer


def get_filters(model, fields):
    
    # Creamos una clase meta y posteriormente le agregamos atributos
    class Meta:
        pass

    init_filters = {}
    for field in fields:
        init_filters[field] = ['icontains', 'exact']

    # Le agregamos a la clase meta el model y los fields
    setattr(Meta, 'model', model)
    setattr(Meta, 'fields', init_filters)

    attrs = {
        'Meta': Meta,
    }

    # Se usa la función TYPE de python para crear el filtro haciendo uso de los attrs creados
    filter = type('model', (filters.FilterSet,), attrs)

    return filter