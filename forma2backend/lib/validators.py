# -*- coding: utf-8 -*-

from django.core.validators import RegexValidator


isalphanumericvalidator = RegexValidator(r'^[a-zA-Z0-9]*$',
                             message='Este campo debe ser alfanumérico.',
                             code='Inválido')

isalphanumericvalidatorwithspace = RegexValidator(r'^[A-Za-zÑñáéíóúÁÉÍÓÚ0-9 \-\–]+$',
                             message='Este campo solo permite números, letras, guiones y espacios.',
                             code='Inválido')

isnumbervalidator = RegexValidator(r'^[0-9]*$',
                             message='Este campo debe ser numérico.',
                             code='Inválido')

isalphavalidator = RegexValidator(r'^[a-zA-Z- ñÑ-áéíóúÁÉÍÓÚ]+$',
                             message='Este campo sólo permite letras.',
                             code='Inválido')

userfidelizavalidator = RegexValidator(r'^[A-Z]{1}\d{6,11}$',
                             message='Éste debe contener el formato establecido para los usuarios de fideliza.',
                             code='Inválido')