'''
Serializadores de la multimedia
'''
from rest_framework import serializers
from app.models import Renessfilter


class RenessfilterSerializer(serializers.ModelSerializer):
    
    is_active = serializers.BooleanField(label='Esta Activo', default=True)
    
    class Meta:
        model = Renessfilter
        # fields = ('id', 'word')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')