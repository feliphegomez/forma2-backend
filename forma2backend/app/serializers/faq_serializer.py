'''
Serializadores de la Preguntas Frecuentes
'''
from rest_framework import serializers
from app.models import FAQQuery

from app.models.category import Category
from django.db.models import Q
from lib.validators import isnumbervalidator


CATEGORIES_LIST = Category.objects.filter(Q(type='FAQ'))

class FAQQuerySerializer(serializers.ModelSerializer):
    #reply = serializers.CharField(label='Respuesta', required=False, allow_null=True, allow_blank=True)
    #category = serializers.ChoiceField(label='category', choices=CATEGORIES_LIST, allow_blank=False, allow_null=True, required=True, write_only=True)
    
    '''
    Serializador de Preguntas Frecuentes
    '''

    is_active = serializers.BooleanField(label='Esta Activo', default=True)
    
    class Meta:
        model = FAQQuery
        # fields = ('id', 'query', 'category', 'reply', )
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')
