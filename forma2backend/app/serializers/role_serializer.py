'''
Serializadores de la role
'''
from rest_framework import serializers
from app.models import Role

class RoleSerializer(serializers.ModelSerializer):
    '''
    Serializador de role
    '''
    
    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Role
        # fields = ('id', 'name')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')
