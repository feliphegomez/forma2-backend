'''
Serializadores de la script
'''
from rest_framework import serializers
from app.models import Script

class ScriptSerializer(serializers.ModelSerializer):
    '''
    Serializador de script
    '''
    
    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Script
        # fields = ('id', 'title', 'script')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')