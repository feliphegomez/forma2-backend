'''
Serializadores de la category
'''
from rest_framework import serializers
from app.models import Category

class CategorySerializer(serializers.ModelSerializer):
    '''
    Serializador de category
    '''

    is_active = serializers.BooleanField(label='Esta Activo', default=True)
    
    class Meta:
        model = Category
        fields = ('id', 'created_at', 'updated_at', 'is_active', 'deleted', 'name', 'type', 'created_by', 'updated_by', 'parent', 'pilots', 'subcategories')
        # fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted', 'subcategories')
