'''
Serializadores de la etiqueta
'''
from rest_framework import serializers
from app.models import Tag

class TagSerializer(serializers.ModelSerializer):	
    '''
    Serializador de etiqueta
    '''
    
    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Tag
        # fields = ('id', 'name')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')