'''
Serializadores de la publish
'''
from rest_framework import serializers
from app.models import Publish

class PublishSerializer(serializers.ModelSerializer):
    '''
    Serializador de publish
    '''
    
    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Publish
        # fields = ('id', 'title', 'link', 'tags', 'picture')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')