'''
Serializadores de la link
'''
from rest_framework import serializers
from app.models import Link

class LinkSerializer(serializers.ModelSerializer):
    '''
    Serializador de link
    '''

    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Link
        # fields = ('id', 'title', 'link')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')