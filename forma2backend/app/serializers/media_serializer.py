'''
Serializadores de la multipicture
'''
from rest_framework import serializers
from app.models import Media


# validators.py
def validate_media_type(temp_media):
    pass


def validate_media_size(temp_media):
    return temp_media.size


class MediaSerializer(serializers.ModelSerializer):
    '''
    Serializador de 
    '''

    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Media
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')
