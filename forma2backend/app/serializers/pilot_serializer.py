'''
Serializadores de la pilotos
'''
from rest_framework import serializers
from app.models import Pilot

class PilotSerializer(serializers.ModelSerializer):
    '''
    Serializador de piloto
    '''

    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Pilot
        # fields = ('id', 'name')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')
