'''
Serializadores de la multimedia
'''
from rest_framework import serializers
from app.models import Notepad


class NotepadSerializer(serializers.ModelSerializer):
    '''
    Serializador de notepad
    '''
    
    is_active = serializers.BooleanField(label='Esta Activo', default=True)

    class Meta:
        model = Notepad
        # fields = ('id', 'title', 'text')
        fields = '__all__'
        read_only_fields = ('created_at', 'updated_at', 'created_by', 'updated_by', 'deleted')