from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel
from django.utils import timezone


class MediaLog(BaseLogModel):
    '''Log modelo de archivos'''
    record = models.ForeignKey('Media', verbose_name='Registro', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_picturelog', 'Consultar log de una archivos'),
                       ('list_picturelog', 'Consultar log de archivos'))


class Media(BaseModel):
    '''
    Modelo de archivos
    '''
    
    log_class = MediaLog
    title = models.CharField(blank=True, null=True, max_length=150)
    description = models.CharField(blank=True, null=True, max_length=500)
    file = models.FileField(
        'Archivo', 
        upload_to='app/media/%Y/%m/%d/', 
        max_length=150, 
        unique=True
    )
    
    def __unicode__(self):
        return self.title

    def __str__(self):
        return str("{0} - {1}".format(self.id, self.title))

    class Meta:
        verbose_name_plural = 'Multipictures'
        verbose_name = 'Multipicture'
        default_permissions = ()
        permissions = (
            ('add_picture', 'Crear una archivos'),
            ('list_picture', 'Consultar archivos'),
            ('retrieve_picture', 'Consultar una archivos'),
            ('change_picture', 'Actualizar una archivos'),
            ('delete_picture', 'Eliminar una archivos'),
        )
