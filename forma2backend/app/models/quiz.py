from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel



class QuizLog(BaseLogModel):
    '''Log modelo de quiz'''
    record = models.ForeignKey('Quiz', verbose_name='Quizs', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_quiz_log', 'Consultar log de una Quiz'),
                       ('list_quiz_log', 'Consultar log de Quizs'))


class Quiz(BaseModel):

    log_class = QuizLog

    title = models.CharField(max_length=150)
    date = models.DateField(auto_now=True)
    enable = models.BooleanField(blank=True, default=0)

    def __str__(self):
        return self.title
