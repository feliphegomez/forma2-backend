from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel



class XxxxxLog(BaseLogModel):
    '''Log modelo de xxxxx'''
    record = models.ForeignKey('Xxxxx', verbose_name='PluralName', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_xxxxxlog', 'Consultar log de una SingularName'),
                       ('list_xxxxxlog', 'Consultar log de SingularNames'))


class Xxxxx(BaseModel):

    log_class = XxxxxLog

    title = models.CharField(max_length=150)
    link = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.title



class XxxxxUserLog(BaseLogModel):
    '''Log modelo de usuarios X SingularNames'''
    record = models.ForeignKey('XxxxxUser', verbose_name='Registro', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_xxxxx_userslog', 'Consultar log de un usuario-SingularName'),
                       ('list_xxxxx_userslog', 'Consultar log de usuarios-SingularNames'))


class XxxxxUser(BaseModel):
    '''
    Modelo intermedio entre SingularNames y usuarios
    '''

    log_class = XxxxxUserLog

    user = models.ForeignKey(User, related_name='xxxxx_user_data', on_delete=models.CASCADE)
    xxxxx = models.ForeignKey(Xxxxx, related_name='xxxxx_users', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'Usuarios - PluralName'
        verbose_name = 'Usuario - SingularName'
        default_permissions = ()
        permissions = (
            ('add_xxxxx_users', 'Crear un usuario - SingularName'),
            ('list_xxxxx_users', 'Consultar usuario - SingularNames'),
            ('retrieve_xxxxx_users', 'Consultar un usuario - SingularName'),
            ('change_xxxxx_users', 'Actualizar un usuario - SingularName'),
            ('delete_xxxxx_users', 'Eliminar un usuario - SingularName'),
        )




