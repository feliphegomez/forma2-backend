from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel
from app.models.pilot import Pilot


CONTENT_TYPE_ECARD = 'ECARD'
CONTENT_TYPE_ARTICLE = 'ARTICLE'
CONTENT_TYPE_FAQ = 'FAQ'

CONTENT_TYPE_LIST = [
    (CONTENT_TYPE_ECARD, 'Categoria para eCard / Banner'),
    (CONTENT_TYPE_ARTICLE, 'Categoria para Articulo'),
    (CONTENT_TYPE_FAQ, 'Categoria para Preguntas Frecuentes'),
]

class CategoryLog(BaseLogModel):
    '''Log modelo de category'''
    record = models.ForeignKey('Category', verbose_name='Categories', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_categorylog', 'Consultar log de una categoria'),
                       ('list_categorylog', 'Consultar log de categorias'))


class Category(BaseModel):
    log_class = CategoryLog

    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name='subcategories')
    name = models.CharField('Nombre de la Categoría', max_length=150, blank=False, null=False)
    type = models.CharField('Tipo de Categoria', choices=CONTENT_TYPE_LIST, max_length=35)
    pilots = models.ManyToManyField(Pilot, blank=True)

    def __str__(self):
        return self.name
