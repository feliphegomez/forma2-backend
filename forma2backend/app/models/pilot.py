from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel



class PilotLog(BaseLogModel):
    '''Log modelo de pilot'''
    record = models.ForeignKey('Pilot', verbose_name='Pilots', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_pilotlog', 'Consultar log de una piloto'),
                       ('list_pilotlog', 'Consultar log de pilotos'))


class Pilot(BaseModel):
    log_class = PilotLog

    name = models.CharField('Nombre del Piloto', max_length=150)

    def __str__(self):
        return self.name
