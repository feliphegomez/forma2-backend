from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel

from django.utils import timezone

class NotepadLog(BaseLogModel):
    '''Log modelo de notepad'''
    record = models.ForeignKey('Notepad', verbose_name='Notas', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_notepadlog', 'Consultar log de una nota'),
                       ('list_notepadlog', 'Consultar log de notas'))


class Notepad(BaseModel):
    log_class = NotepadLog

    title = models.CharField('Titulo',max_length=150, null=False)
    text = models.TextField('Contenido', max_length= 1000, blank=True, null=True, default='')

    def __str__(self):
        return self.title

