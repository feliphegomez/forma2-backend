from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel


class LinkLog(BaseLogModel):
    '''Log modelo de link'''
    record = models.ForeignKey('Link', verbose_name='Links', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_linklog', 'Consultar log de un Link'),
                       ('list_linklog', 'Consultar log de los Links'))


class Link(BaseModel):
    log_class = LinkLog

    title = models.CharField('Titulo', max_length=150, unique=True)
    link = models.URLField('Link', blank=True, null=True, unique=True)

    def __str__(self):
        return self.title
