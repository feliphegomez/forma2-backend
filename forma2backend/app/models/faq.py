from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel

from app.models.category import Category

class FAQQueryLog(BaseLogModel):
    '''Log modelo de faq_query'''
    record = models.ForeignKey('FAQQuery', verbose_name='FAQs Querys', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_faq_querylog', 'Consultar log de una FAQ Query'),
                       ('list_faq_querylog', 'Consultar log de FAQ Querys'))


class FAQQuery(BaseModel):
    log_class = FAQQueryLog

    query = models.CharField(max_length=350, unique=False)
    reply = models.CharField(max_length=350, blank=True, default='Esperando respuesta...')
    category = models.ForeignKey(Category, unique=False, on_delete=models.PROTECT)
        
    def __str__(self):
        return "%s : %s" % (self.query, self.reply)

