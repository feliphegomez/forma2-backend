from django.db import models
from django.contrib.auth.models import User, Permission
from src.model import BaseModel, BaseLogModel


class RoleLog(BaseLogModel):
    '''Log modelo de role'''
    record = models.ForeignKey('Role', verbose_name='Roles', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_rolelog', 'Consultar log de una rol'),
                       ('list_rolelog', 'Consultar log de roles'))


class Role(BaseModel):
    log_class = RoleLog

    name = models.CharField('Nombre del Rol', max_length=150)
    permissions = models.ManyToManyField(Permission, blank=True, related_name='permissions')

    def __str__(self):
        return self.name
