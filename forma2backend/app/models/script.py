from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel


class ScriptLog(BaseLogModel):
    '''Log modelo de script'''
    record = models.ForeignKey('Script', verbose_name='Scripts', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_scriptlog', 'Consultar log de una script'),
                       ('list_scriptlog', 'Consultar log de scripts'))


class Script(BaseModel):
    log_class = ScriptLog

    title = models.CharField('Titulo', max_length=150, blank=False, null=False, unique=True)
    script = models.TextField('Guion', blank=False, null=False, max_length=100000, unique=True)

    def __str__(self):
        return self.title
