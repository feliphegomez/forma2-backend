from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel


class TagLog(BaseLogModel):
    '''Log modelo de etiquetas'''
    record = models.ForeignKey('Tag', verbose_name='Etiquetas', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_taglog', 'Consultar log de una etiqueta'),
                       ('list_taglog', 'Consultar log de etiquetas'))


class Tag(BaseModel):
    '''
    Modelo de etiquetas
    '''
    
    log_class = TagLog
    name = models.CharField('Etiqueta', max_length=150, unique=True, null=False, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Etiquetas'
        verbose_name = 'Etiqueta'
        default_permissions = ()
        permissions = (
            ('add_tag', 'Crear una etiqueta'),
            ('list_tag', 'Consultar etiquetas'),
            ('retrieve_tag', 'Consultar una etiqueta'),
            ('change_tag', 'Actualizar una etiqueta'),
            ('delete_tag', 'Eliminar una etiqueta'),
        )
