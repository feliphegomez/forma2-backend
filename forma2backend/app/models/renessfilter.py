from django.db import models
from django.contrib.auth.models import User
from src.model import BaseModel, BaseLogModel


class RenessfilterLog(BaseLogModel):
    '''Log modelo de renessfilter'''
    record = models.ForeignKey('Renessfilter', verbose_name='Palabras', on_delete=models.PROTECT)

    class Meta:
        default_permissions = ()
        permissions = (('retrieve_renessfilterlog', 'Consultar log de una palabra'),
                       ('list_renessfilterlog', 'Consultar log de palabras'))


class Renessfilter(BaseModel):
    log_class = RenessfilterLog

    word = models.CharField('Palabra a bloquear', max_length=150, unique=True, blank=False, null=False)

    def __str__(self):
        return self.word
