'''
Vista para las notepad 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Notepad
from app.serializers import NotepadSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import NotepadFilter


class NotepadView(BaseViewSet):
    '''
    Vista de notepad
    '''

    permission_code = 'notepad'
    queryset = Notepad.objects.all()
    serializer_class = NotepadSerializer
    filter_class = NotepadFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)