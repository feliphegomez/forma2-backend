'''
Vista para las publicaciones 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Publish
from app.serializers import PublishSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import PublishFilter


class PublishView(BaseViewSet):
    '''
    Vista de publicaciones
    '''

    permission_code = 'publish'
    queryset = Publish.objects.all()
    serializer_class = PublishSerializer
    filter_class = PublishFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)