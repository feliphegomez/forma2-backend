'''
Vista para las multimedia 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Media
from app.serializers import MediaSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import MediaFilter


class MediaView(BaseViewSet):
    '''
    Vista de multimedia
    '''

    permission_code = 'media'
    queryset = Media.objects.all()
    serializer_class = MediaSerializer
    filter_class = MediaFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)