'''
Vista para las link 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Link
from app.serializers import LinkSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import LinkFilter


class LinkView(BaseViewSet):
    '''
    Vista de link
    '''

    permission_code = 'link'
    queryset = Link.objects.all()
    serializer_class = LinkSerializer
    filter_class = LinkFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)