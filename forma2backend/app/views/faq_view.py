'''
Vista para las Preguntas Frecuentes 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import FAQQuery
from app.serializers import FAQQuerySerializer
from src.view import BaseViewSet, validate_permission
from app.filters import FAQQueryFilter



class FAQQueryView(BaseViewSet):
    '''
    Vista de preguntas frecuentes
    '''

    permission_code = 'faq_query'
    queryset = FAQQuery.objects.all()
    serializer_class = FAQQuerySerializer
    filter_class = FAQQueryFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter, )
