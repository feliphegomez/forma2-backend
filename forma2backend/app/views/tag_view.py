'''
Vista para las etiquetas 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Tag
from app.serializers import TagSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import TagFilter


class TagView(BaseViewSet):
    '''
    Vista de etiqueta
    '''

    permission_code = 'tag'
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filter_class = TagFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)