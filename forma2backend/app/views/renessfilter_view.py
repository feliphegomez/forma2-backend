'''
Vista para las renessfiltrer 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Renessfilter
from app.serializers import RenessfilterSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import RenessfilterFilter


class RenessfilterView(BaseViewSet):
    '''
    Vista de renessfiltrer
    '''

    permission_code = 'renessfilter'
    queryset = Renessfilter.objects.all()
    serializer_class = RenessfilterSerializer
    filter_class = RenessfilterFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
