'''
Vista para las script 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Script
from app.serializers import ScriptSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import ScriptFilter


class ScriptView(BaseViewSet):
    '''
    Vista de script
    '''

    permission_code = 'script'
    queryset = Script.objects.all()
    serializer_class = ScriptSerializer
    filter_class = ScriptFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)