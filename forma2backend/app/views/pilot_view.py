'''
Vista para las pilotos
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Pilot
from app.serializers import PilotSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import PilotFilter


class PilotView(BaseViewSet):
    '''
    Vista de piloto
    '''

    permission_code = 'pilot'
    queryset = Pilot.objects.all()
    serializer_class = PilotSerializer
    filter_class = PilotFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)