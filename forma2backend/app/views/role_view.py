'''
Vista para las role 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Role
from app.serializers import RoleSerializer
from src.view import BaseViewSet, validate_permission
from app.filters import RoleFilter


class RoleView(BaseViewSet):
    '''
    Vista de role
    '''

    permission_code = 'role'
    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    filter_class = RoleFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)