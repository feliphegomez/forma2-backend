'''
Vistas de la aplicación
'''
from .pilot_view import *

from .category_view import CategoryView
from .tag_view import TagView
from .media_view import MediaView
from .link_view import LinkView
from .notepad_view import NotepadView
from .renessfilter_view import RenessfilterView
from .publish_view import PublishView
from .script_view import ScriptView
from .index_view import index
from .faq_view import *