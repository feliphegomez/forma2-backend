'''
Vista para las Categorias 
'''

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action

from src.create_models import GenerateModel
from src.exceptions import (NoFieldError, CharacterError, DuplicateError)

from app.models import Category
from app.serializers import CategorySerializer
from src.view import BaseViewSet, validate_permission
from app.filters import CategoryFilter


class CategoryView(BaseViewSet):
    '''
    Vista de Categorias
    '''

    permission_code = 'category'
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_class = CategoryFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter, )