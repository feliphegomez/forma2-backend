'''
Filtros de publicaciones
'''
from django_filters import rest_framework as filters
from app.models import Publish


class PublishFilter(filters.FilterSet):
    '''
    Filtro para las publicaciones
    '''

    class Meta:
        model = Publish
        fields = {
            'id': ['icontains', 'exact'],
            'title': ['icontains', 'exact'],
            'tags': ['icontains', 'exact']
        }