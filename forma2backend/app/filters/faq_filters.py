'''
Filtros de xxxxxx
'''
from django_filters import rest_framework as filters
from app.models import FAQQuery


class FAQQueryFilter(filters.FilterSet):
    '''
    Filtro para las xxxxxx
    '''

    class Meta:
        model = FAQQuery
        fields = {
            'id': ['icontains', 'exact'],
            'query': ['icontains', 'exact'],
            #'category': ['icontains', 'exact'],
            'reply': ['icontains', 'exact'],
        }
