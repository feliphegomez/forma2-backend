'''
Filtros de multipicture
'''
from django_filters import rest_framework as filters
from app.models import Media


class MediaFilter(filters.FilterSet):
    '''
    Filtro para las multipicture
    '''

    class Meta:
        model = Media
        fields = {
            'id': ['icontains', 'exact']
        }