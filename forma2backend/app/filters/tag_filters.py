'''
Filtros de etiqueta
'''
from django_filters import rest_framework as filters
from app.models import Tag


class TagFilter(filters.FilterSet):
    '''
    Filtro para las etiquetas
    '''

    class Meta:
        model = Tag
        fields = {
            'id': ['icontains', 'exact'],
            'name': ['icontains', 'exact']
        }