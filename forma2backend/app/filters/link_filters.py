'''
Filtros de link
'''
from django_filters import rest_framework as filters
from app.models import Link


class LinkFilter(filters.FilterSet):
    '''
    Filtro para las link
    '''

    class Meta:
        model = Link
        fields = {
            'id': ['icontains', 'exact'],
            'title': ['icontains', 'exact'],
            'link': ['icontains', 'exact']
        }