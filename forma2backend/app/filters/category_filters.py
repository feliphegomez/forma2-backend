'''
Filtros de category
'''
from django_filters import rest_framework as filters
from app.models import Category


class CategoryFilter(filters.FilterSet):
    '''
    Filtro para las category
    '''

    class Meta:
        model = Category
        fields = {
            'id': ['icontains', 'exact'],
            'name': ['icontains', 'exact'],
            'type': ['icontains', 'exact'],
        }