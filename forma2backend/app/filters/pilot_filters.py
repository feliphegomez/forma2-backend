'''
Filtros de pilot
'''
from django_filters import rest_framework as filters
from app.models import Pilot


class PilotFilter(filters.FilterSet):
    '''
    Filtro para las pilot
    '''

    class Meta:
        model = Pilot
        fields = {
            'id': ['icontains', 'exact'],
            'name': ['icontains', 'exact']
        }