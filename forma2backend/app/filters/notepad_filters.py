'''
Filtros de notepad
'''
from django_filters import rest_framework as filters
from app.models import Notepad


class NotepadFilter(filters.FilterSet):
    '''
    Filtro para las notepad
    '''

    class Meta:
        model = Notepad
        fields = {
            'id': ['icontains', 'exact'],
            'title': ['icontains', 'exact'],
            'text': ['icontains', 'exact']
        }