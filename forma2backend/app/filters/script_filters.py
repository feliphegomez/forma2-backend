'''
Filtros de script
'''
from django_filters import rest_framework as filters
from app.models import Script


class ScriptFilter(filters.FilterSet):
    '''
    Filtro para las script
    '''

    class Meta:
        model = Script
        fields = {
            'id': ['icontains', 'exact']
        }