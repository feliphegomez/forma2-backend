'''
Filtros de role
'''
from django_filters import rest_framework as filters
from app.models import Role


class RoleFilter(filters.FilterSet):
    '''
    Filtro para las role
    '''

    class Meta:
        model = Role
        fields = {
            'id': ['icontains', 'exact'],
            'name': ['icontains', 'exact']
        }