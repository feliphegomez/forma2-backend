'''
Filtros de filtro de palabras
'''
from django_filters import rest_framework as filters
from app.models import Renessfilter


class RenessfilterFilter(filters.FilterSet):
    '''
    Filtro para las renessfilter
    '''

    class Meta:
        model = Renessfilter
        fields = {
            'id': ['icontains', 'exact'],
            'word': ['icontains', 'exact']
        }