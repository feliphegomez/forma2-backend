"""
    forma2backend URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework.routers import DefaultRouter
from app.views import *
from rest_framework_jwt.views import verify_jwt_token

from django.views.static import serve
from rest_framework.documentation import include_docs_urls

from user_data.views import *


router = DefaultRouter()

router.register(r'pilots', PilotView, base_name='pilots')
router.register(r'roles', GroupViewSet, base_name='roles')
router.register(r'users', UserViewSet, base_name='users')

router.register(r'categories', CategoryView, base_name='categories')
router.register(r'FAQs/querys', FAQQueryView, base_name='FAQs/querys')
router.register(r'links', LinkView, base_name='links')
router.register(r'notepads', NotepadView, base_name='notepads')
router.register(r'medias', MediaView, base_name='medias')
router.register(r'publishs', PublishView, base_name='publishs')
router.register(r'renessfilters', RenessfilterView, base_name='renessfilters')
router.register(r'scripts', ScriptView, base_name='scripts')
router.register(r'tags', TagView, base_name='tags')

#router.register(r'permissions', PermissionViewSet, base_name='permissions')
#router.register(r'users-full', UserFullViewSet, base_name='users-full')

urlpatterns = [
    path('', include('app.urls')),
    #path('', include('user_data.urls')),
    url(r'^docs/', include_docs_urls(
        title='Forma2 Fg',
        authentication_classes=(),
        permission_classes=())),
        url(r'^admin/',
        admin.site.urls
    ),
    url(r'^api/', include(router.urls)),
    url(r'^api/login/', obtain_jwt_token),
    url(r'^api/check-token/', verify_jwt_token),
    url(r'^api/medias/([0-9]+)/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT,}),
    url(r'^api/medias/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT,}),
    # url(r'^api/assignament/', Assignament.as_view()),
    # url(r'^api/update-data-source/', UpdateStatusDataSource.as_view()),
    # url(r'^api/get-management-history/', LogManagement.as_view()),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
