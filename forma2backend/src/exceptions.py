class DynamicModelError(Exception):
    """
    Clase base para las demás excepciones generadas por los modelos dinámicos
    """

    def __init__(self):
        '''
        Método contructor encargado de la inicialización de atributos
        '''
        
        message = self.message
    pass


class CharacterError(DynamicModelError):
    """
    Clase para la excepción generada por el ingreso de caracteres
    especiales en el JSON.
    """

    def __init__(self, model):
        self.message = 'Caracter especial ingresado ' + str(model) + ', por favor verifique e inténtelo de nuevo.'

    pass


class NoFieldError(DynamicModelError):
    """
    Clase para la excepción generada por la entrada vacía de algún
    campo en el JSON.
    """

    message = 'Error al ingresar los campos del modelo, por favor verifique e inténtelo de nuevo.'
    pass


class NoModelNameError(DynamicModelError):
    """
    Clase para la excepción generada por la entrada vacía del nombre
    del modelo en el JSON.
    """

    message = 'El modelo a crear carece de nombre, por favor verifique e inténtelo de nuevo.'
    pass


class DuplicateError(DynamicModelError):
    """
    Clase para la excepción generada por la existencia previa del
    modelo.
    """

    message = 'La campaña que pretende crear ya existe, por favor verifique e inténtelo de nuevo.'
    pass


class MassiveChargeError(Exception):
    """
    Clase base para las demás excepciones generadas por la carga masiva
    """

    def __init__(self):
        '''
        Método contructor encargado de la inicialización de atributos
        '''
        
        message = self.message
    pass


class UnexsistanceError(MassiveChargeError):
    """
    Clase para la excepción generada por la inexistencia de la tabla a
    la cual se le intenta realizar la carga masiva
    """

    message = 'Tabla inexistente en la base de datos, por favor verifique e inténtelo de nuevo.'
    pass


class ParityError(MassiveChargeError):
    """
    Clase para la excepción generada por la disparidad entre campos de
    la tabla en base de datos y el header de las columnas en el archivo
    Excel.
    """

    message = 'Error en las columnas ingresadas en el archivo, no concuerdan con los campos de la campaña seleccionada, por favor verifique e inténtelo de nuevo.'
    pass


class FileFormatError(MassiveChargeError):
    """
    Clase para la excepción generada por el ingreso de un archivo que 
    carece de las extensiones permitidas.
    """

    message = 'Error en el formato del archivo ingresado, por favor verifique e inténtelo de nuevo.'
    pass


class ContentFileError(MassiveChargeError):
    """
    Clase para la excepción generada por la carga de un archivo con
    contenido defectuoso o no indicado.
    """

    message = 'Error en el contenido del archivo ingresado, por favor verifique e inténtelo de nuevo.'
    pass


class EmptyFileError(MassiveChargeError):
    """
    Clase para la excepción generada por la carga de un archivo vacío.
    """

    message = 'Error. El archivo ingresado carece de contenido, por favor verifique e inténtelo de nuevo.'
    pass

class WrongParamError(MassiveChargeError):
    """
    Clase para la excepción generada por el ingreso de parámetros erróneos en la instancia de la clase MassiveCharge.
    """

    message = 'Error, los parámetros ingresados a la clase son erróneos.'
    pass