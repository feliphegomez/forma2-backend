'''
Módulo para la carga masiva de datos provenientes de un archivo Excel
'''

import csv
import datetime
import io
import MySQLdb
import re
import pyodbc 
import requests
import xlrd
from django.db import connection
from django.conf import settings
from django.contrib.auth.models import User, Group
from openpyxl import load_workbook
from user_data.models import UserData
from .exceptions import (UnexsistanceError, ParityError, FileFormatError, ContentFileError, EmptyFileError, WrongParamError)

class MassiveCharge:
    '''
    Clase para la carga masiva de datos.
    '''

    def __init__(self,
                file,
                file_name,
                charge_target,
                optional_fields=[], 
                default_data=[], 
                not_file_fields=[],
                table_label=None,
                allowed_char_pattern='[^ a-zA-Z0-9 ÑñáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ]',
                fill_per_reg=False,
                overwrite_option=None,
                overwrite_field=None
                ):
        '''
        Método constructor encargado de inicializar atributos y ejecutar
        el método execute.

        Parámetros:
        file -- Archivo ingresado 
        file_name -- Nombre de la tabla para realizar la carga masiva
        charge_target -- Blanco de la carga, modelo en particular o usuarios
        optional_fields = Lista con campos adicionales a agregar
        default_data -- Lista con los valores por defecto para los campos opiconales 
        not_file_fields -- Lista de campos que no se encuentran en el archivo pero si en la BD
        table_label -- Etiqueta que puedan contener las tablas
        allowed_char_pattern -- Caracteres permitidos en los archivos ingresados
        fill_per_reg -- Indica la forma de cargar en la BD, 1 petición o 'n' peticiones a esta
        overwrite_option -- Opción de sobre-escritura
        overwrite_field -- Campo por el cual sobre-escribir (de seleccionar 'por campo')
        '''
        self.file = file
        self.file_name = str(self.file)
        self.charge_target = charge_target
        self.name_table = table_label + file_name
        self.allowed_char_pattern = allowed_char_pattern
        self.fill_per_reg = fill_per_reg

        self.optional_fields = optional_fields
        self.default_data = default_data
        self.not_file_fields = not_file_fields
        self.overwrite_option = overwrite_option
        self.overwrite_field = overwrite_field
        self.input_name_table = file_name

        # Validación de cantidad de elementos para campos opcionales
        if len(self.optional_fields) != len(self.default_data):
            raise WrongParamError
        
        # Si la expresión regular contiene ";", esta es suprimida
        # Debido a los fallos presentados en archivos CSV
        if re.search('[;]', self.allowed_char_pattern):
            self.allowed_char_pattern = self.allowed_char_pattern.replace(';', ' ')

        # Contador de iteraciones usado para los archivos csv o xls (lazy file)
        self.iterator_count = 0
        # Variables para el volcado en caso de ser un archivo csv o xls (lazy file)
        self.file_content = []
        self.file_fields = []
        # Variable para el almacenamiento del número de columnas del archivo
        self.num_fields = 0

        # Obtención del engine usado en la configuración de la BD
        self. db_engine = (settings.DATABASES['default']['ENGINE'])
        # Conexión con la base de datos mediante el método config_data_base
        self.db_conn = self.config_data_base()
        
        self.execute()
        
    def config_data_base(self):
        '''
        Método encargado de realizar la conexión con el SGBD de acuerdo
        a la configuración DATABASES en el archivo settings.
        
        Retorna la respectiva conexión.
        '''
        # Si el SGBD es Mysql se realiza su respectiva conexión
        if self.db_engine == 'django.db.backends.mysql':
            return (MySQLdb.connect(user=settings.DATABASES['default']['USER'], 
                                    db=settings.DATABASES['default']['NAME'], 
                                    passwd=settings.DATABASES['default']['PASSWORD'], 
                                    host=settings.DATABASES['default']['HOST']))

        # Si el SGBD es SQL Server se realiza su respectiva conexión        
        elif self.db_engine == 'sql_server.pyodbc':
            conn_str = (      
            r'DRIVER={{ODBC Driver 13 for SQL Server}};'
            r'SERVER={};'
            r'DATABASE={};'
            r'UID={};'
            r'PWD={}'
            ).format(settings.DATABASES['default']['HOST'],
                    settings.DATABASES['default']['NAME'], 
                    settings.DATABASES['default']['USER'], 
                    settings.DATABASES['default']['PASSWORD'])

            return pyodbc.connect(conn_str)

    def charge_xlsx(self):
        '''
        Método encargado de adaptar la información proveniente de
        archivos .xlsx, .xlsm, .xlst, .xltm
        
        Retorna diccionario con los headers y el contenido del archivo.
        '''
        # Si es la primer lectura realiza la obtención de información del archivo
        if self.iterator_count == 0:
            # Lectura del archivo
            book = load_workbook(filename=self.file, read_only=True)
            # Toma la primer hoja del archivo Excel para la realización de la carga masiva
            worksheet = book[book.sheetnames[0]]

            # Validación de contenido nulo
            if worksheet.max_row == 1:
                raise EmptyFileError

            # Variable que contendrá la columna de partida
            starting_row_num = 1
            data_content = []
            empty = True

            # Obtención de los headers de columnas de la tabla
            while empty == True:
                for row in worksheet.rows:
                    data_fields = []

                    #Es llenada una lista con los datos de la correspondiente fila
                    for cell in row:
                        data_fields.append(cell.value)

                    # Si la totalidad de elementos de la lista de headers son None
                    # se debe realizar de nuevo el recorrido usando otra fila 
                    if data_fields.count(None) == len(data_fields):
                        starting_row_num += 1
                    # Cuando se encuentren headers válidos se termina el ciclo 
                    else:
                        empty = False
                        break

            # Son removidos los campos sobrantes vacíos
            new_data_fields = []
            for header in data_fields:
                if header != None:
                    new_data_fields.append(header)

            # Variable que contendrá la fila de partida    
            starting_col_num = worksheet.max_column-len(new_data_fields) 

            # Volcado de variable para uso posterior
            self.num_fields = len(new_data_fields)
            current_row = 0

            # Obtención de contenido            
            for row in worksheet.rows:
                data_row = []
                if current_row < starting_row_num:
                    current_row += 1
                else:
                    # Recorrido de sólo contenido
                    for current_col, cell in enumerate(row, 1):
                        if current_col >= (starting_col_num + 1):
                            # Para evitar problemas con las fechas se inserta la información como Str
                            if cell.value == None:
                                data_row.append(cell.value)                         
                            else:
                                data_row.append(str(cell.value))

                    # Eliminación de posibles filas vacías que pueda contener
                    if data_row.count(None) != len(new_data_fields):
                        # Si sólo existe una columna en el archivo
                        if len(new_data_fields) == 1:
                            data_content.append(data_row[0])
                        # Si existen varias columnas en el archivo
                        else:
                            data_content.append(tuple(data_row))                    

            # Adaptación de los campos mediante el método change_content
            data_content = self.change_content(data_content)
            new_data_fields = self.change_header(new_data_fields)

            if self.charge_target != 'USER':
                # Adición de campos opcionales adicionales
                optional_data = self.add_optional_data(data_content, new_data_fields)
                new_data_fields = optional_data['fields']
                data_content = optional_data['content']
            
            # Volcado de variables para futuros llamados del método
            self.file_content = data_content
            self.file_fields = new_data_fields

        else:
            data_content = self.file_content
            new_data_fields = self.file_fields

        self.iterator_count += 1

        return {'fields':new_data_fields, 'content':data_content}

    def charge_xls(self):
        '''
        Método encargado de adaptar la información proveniente de
        archivos .xls
        
        Retorna diccionario con los headers y el contenido del archivo.
        '''
        # Si es la primer lectura realiza la obtención de información del archivo
        if self.iterator_count == 0:
            # Lectura del archivo        
            book = xlrd.open_workbook(filename=None, file_contents=self.file.read(), on_demand=True)

            # Toma la primer hoja del archivo Excel para la realización de la carga masiva
            worksheet = book.sheet_by_index(0)

            # Validación de contenido nulo
            if worksheet.nrows == 0:
                raise EmptyFileError    
  
            data_content =[]
            empty = True

            # Variable que contendrá la columna de partida
            starting_row_num = 0
            while empty == True:
                data_fields = []

                # Es llenada una lista con los datos de la correspondiente fila
                for col in range(worksheet.ncols):
                    data_fields.append( worksheet.cell_value(starting_row_num,col))

                # Si la totalidad de elementos de la lista de headers son None
                # se debe realizar de nuevo el recorrido usando otra fila
                if data_fields.count('') == len(data_fields):
                    starting_row_num += 1
                # Cuando se encuentren headers válidos se termina el ciclo 
                else:
                    empty = False

            # Son removidos los campos sobrantes vacíos
            new_data_fields = []
            for header in data_fields:
                if header != '':
                    new_data_fields.append(header)

            # Variable que contendrá la fila de partida    
            starting_col_num = worksheet.ncols-len(new_data_fields)

            # Volcado de variable para uso posterior
            self.num_fields = len(new_data_fields)

            # Obtención de contenido
            for row in range(starting_row_num+1, worksheet.nrows):
                data_row = []
                for col in range(starting_col_num, worksheet.ncols):
                    # Recorrido de datos en busca de contenido vacío para ser remplazado por None
                    if worksheet.cell(row,col).value == '':
                        data_row.append(None)
                    else:
                        # Cambio de float a date en fechas (sólo sucede para xlrd)
                        if worksheet.cell(row,col).ctype == 3:
                            date = worksheet.cell(row,col)
                            year, month, day, hour, minute, second = xlrd.xldate_as_tuple(date.value, book.datemode)
                            new_date = str(year)+'-'+str(month)+'-'+str(day)
                            data_row.append(new_date)           
                        else:
                            data_row.append(worksheet.cell_value(row,col))

                # Elimina posibles filas vacías que pueda contener
                if data_row.count(None) != len(data_fields):
                    # Si sólo existe una columna en el archivo
                    if len(new_data_fields) == 1:
                        data_content.append(data_row[0])
                    # Si existen varias columnas en el archivo
                    else:
                        data_content.append(tuple(data_row))

            # Adaptación de los campos mediante el método change_content        
            data_content = self.change_content(data_content)
            new_data_fields = self.change_header(new_data_fields)

            if self.charge_target != 'USER': 
                # Adición de campos opcionales adicionales
                optional_data = self.add_optional_data(data_content, new_data_fields)
                new_data_fields = optional_data['fields']
                data_content = optional_data['content']

            # Volcado de variables para futuros llamados del método
            self.file_content = data_content
            self.file_fields = new_data_fields

        else:
            data_content = self.file_content
            new_data_fields = self.file_fields

        self.iterator_count += 1
    
        return {'fields':new_data_fields, 'content':data_content}

    def charge_csv(self):
        '''
        Método encargado de adaptar la información proveniente de
        archivos .csv
        
        Retorna diccionario con los headers y el contenido del archivo.
        '''
        data_fields = []
        data_content = []

        # Si es la primer lectura realiza la obtención de información del archivo
        if self.iterator_count == 0:
            # decoded_file = self.file.read().decode('utf-8')
            decoded_file = self.file.read().decode('cp1252')
            io_string = io.StringIO(decoded_file)

            for index, line in enumerate(csv.reader(io_string, delimiter=';', quotechar='|')):
                # Obtención de los headers de columnas de la tabla           
                if index == 0:
                    data_fields = line
                # Obtención de contenido            
                else: 
                    data_row = []
                    for data in line:
                        # Recorrido de datos en busca de contenido vacío para ser remplazado por None                        
                        if data == '':
                            data = None
                        data_row.append(data)

                    # Elimina posibles filas vacías que pueda contener
                    if data_row.count(None) != len(data_fields):
                        # Si sólo existe una columna en el archivo
                        if len(data_fields) == 1:
                            data_content.append(data_row[0])
                        # Si existen varias columnas en el archivo
                        else:
                            data_content.append(tuple(data_row))

            # Volcado de variable para uso posterior
            self.num_fields = len(data_fields)

            # Adaptación de los campos mediante el método change_content
            data_content = self.change_content(data_content)
            data_fields = self.change_header(data_fields)

            if self.charge_target != 'USER': 
                # Adición de campos opcionales adicionales
                optional_data = self.add_optional_data(data_content, data_fields)
                data_fields = optional_data['fields']
                data_content = optional_data['content']

            # Volcado de variables para futuros llamados del método
            self.file_content = data_content
            self.file_fields = data_fields
           
        # Si no es la primer lectura toma los datos provenientes de la primera
        else:
            data_content = self.file_content
            data_fields = self.file_fields

        self.iterator_count += 1

        return {'fields':data_fields, 'content':data_content}     

    def add_optional_data(self, content, fields):
        '''
        Método encargado de agregar nuevos campos y contenido de manera
        opcional.
        
        Parámetros:
        content -- Lista con el contenido (sin headers) del archivo.
        fields -- Lista con los campos del archivo.

        Retorna diccionario con los headers y el contenido del archivo.
        '''
        # Campos a incluir por regla de negocio
        new_fields = self.optional_fields
        default_value = self.default_data

        count_new_fields = 0
        # Adición de nuevos campos
        for field in new_fields:
            # Si el campo a incluir no se encuentra ya, es agregado
            if not field in fields:
                count_new_fields += 1
                fields.append(field)

        # Adición de nuevo contenido para los nuevos campos
        new_content = []
        # Valor por defecto que tendrán el contenido.
        if self.num_fields <= 1:
            for inf in content:
                new_row = [inf]
                for data in range(count_new_fields):
                    new_row.append(default_value[data])
                new_content.append(tuple(new_row))
            return {'fields':fields, 'content':new_content} 
        else:
            for row in content:
                new_row = list(row)
                for data in range(count_new_fields):
                    new_row.append(default_value[data])
                new_content.append(tuple(new_row))

            return {'fields':fields, 'content':new_content} 

    def change_content(self, content):
        '''
        Método encargado de adaptar el contenido del archivo.

        Parámetros:
        content -- Lista con el contenido (sin headers) del archivo.

        Retorna una nueva lista con el contenido del archivo adaptado
        para su ingreso en la base de datos.
        '''
        new_data_content = []        
        # Patrón para la identificación de fechas
        date_pattern = '\d{2}[-\/]\d{2}[-\/]\d{4}'
        # Patrón para la identificación de números decimales        
        num_pattern = '.*[0-9][,][0-9]'

        # Si sólo existe una columna en el archivo
        if self.num_fields == 1:
            for data in content:
                # Búsqueda de datos que contengan el patrón fecha
                if re.search(date_pattern, str(data)):
                    # Remplazamiento de caracteres
                    data = data.replace('/','-')
                    # Cambio de formato para fechas
                    data = datetime.datetime.strptime(data, ('%d-%m-%Y')).strftime('%Y-%m-%d')
                    new_data_content.append(data)               
                # Búsqueda de datos que contengan el patrón para números decimales
                elif re.search(num_pattern, str(data)):
                    # Remplazamiento de caracteres                    
                    data = data.replace(',','.')
                    new_data_content.append(data)
                else:
                    new_data_content.append(data)
                
        # Si el archivo consta de varias columnas
        else:
            # Recorrido del contenido
            for row in content:
                new_data_row = []
                for data in row:
                    # Búsqueda de datos que contengan el patrón fecha
                    if re.search(date_pattern, str(data)):
                        # Remplazamiento de caracteres
                        data = data.replace('/','-')
                        # Cambio de formato para fechas
                        data = datetime.datetime.strptime(data, ('%d-%m-%Y')).strftime('%Y-%m-%d')
                        new_data_row.append(data)
                    # Búsqueda de datos que contengan el patrón para números decimales    
                    elif re.search(num_pattern, str(data)):
                        # Remplazamiento de caracteres
                        data = data.replace(',','.')
                        new_data_row.append(data)
                    else:
                        new_data_row.append(data)

                new_data_content.append(tuple(new_data_row))   

        return new_data_content

    def change_header(self, fields):
        '''
        Método encargado de adaptar los headers del archivo.

        Parámetros:
        fields -- Lista con los headers del archivo.

        Retorna una nueva lista con los headers del archivo adaptados.
        '''
        # Si sólo existe una columna en el archivo
        if self.num_fields == 1:
            # Cambio de espacios por underscore en el nombre de los headers si encuentra espacios
            fields[0] = (str(fields[0]).lower()).replace(' ', '_')

        # Si el archivo consta de varias columnas
        else:
            # Cambio de espacios por underscore en el nombre de los headers si encuentra espacios
            for index, field in enumerate(fields):
                fields[index] = (field.lower()).replace(' ', '_')
        return fields

    def fill_db(self, content, fields):
        '''
        Método encargado de realizar la carga masiva de información
        en la base de datos.

        Parámetros:
        content -- Lista con el contenido del archivo.
        fields -- Lista con los headers del archivo.
        '''
        # Preparación del objeto cursor usando el método cursor()
        cursor = self.db_conn.cursor()
        current_reg = False

        # Preparación de la tupla/string con los nombre de los campos
        # No se usa self.num_fields debido a la posibilidad de nuevos campos añadidos
        if len(fields) == 1:
            name_fields = '(' + str(fields[0]) + ')'
        else:
            if self.db_engine == 'django.db.backends.mysql':
                name_fields = str(tuple(fields)).replace('\'', '`')
            elif self.db_engine == 'sql_server.pyodbc':
                name_fields = str(tuple(fields)).replace('\'', '')

        # Preparación del string (%s) que contendrá VALUES()
        char_accum = ''
        for i in range (len(fields)):
            # El acumulador de caracteres difiere de acuerdo al SGBD
            if self.db_engine == 'django.db.backends.mysql':
                char_accum += '%s'
            elif self.db_engine == 'sql_server.pyodbc':
                char_accum += '?'

            if i+1 < (len(fields)):
                char_accum += ', '

        # SOBREESCRITURA DE INFORMACION

        # String con los campos a sobre-escribir sin contar not_file_fields
        fin_fields = str(fields).replace('[', '').replace(']', '').replace('\'', '')

        # Sobre-escritura de información completa
        if self.overwrite_option == 'FULL' and self.validate_empty_table() != 0:
            current_reg = True
            try:
                # Realización de la carga de información en la segunda tabla
                with connection.cursor() as cursor_ow:
                    # Contrucción del query para la el volcado de datos            
                    sql_statement = 'INSERT INTO db_backup_data_source_{0} ({1}) SELECT {1} FROM db_data_source_{0}'.format(self.input_name_table, fin_fields)
                    # Ejecución del comando statenment
                    cursor_ow.execute(sql_statement)
                    # Se confirma la acción y se ejecuta en la base de datos                                                
                    self.db_conn.commit()

                # Realización del eliminado de información de la primer tabla
                with connection.cursor() as cursor_ow:       
                    # Contrucción del query para la el borrado de datos                                    
                    sql_statement = 'DELETE FROM {}'.format(self.name_table)                    
                    # Ejecución del comando statenment                    
                    cursor_ow.execute(sql_statement)
                    # Se confirma la acción y se ejecuta en la base de datos                            
                    self.db_conn.commit()

            except Exception:
                self.db_conn.rollback()
                raise ContentFileError

        # Sobre-escritura de información por campo
        elif self.overwrite_option == 'PER FIELD' and self.validate_empty_table() != 0:
            current_reg = True            
            field_index = None
            list_field_ow = []

            # Búsqueda en el contenido del campo por sobre-escribir
            for index, field in enumerate(fields):
                if self.overwrite_field.lower() == field.lower():
                    field_index = index
                    break

            # Construcción de lista para evaluar al momento de borrar
            for index, row in enumerate(content):
                # Se evita añadir elementos duplicados para miniminzar el número de querys
                if not row[field_index] in list_field_ow:
                    list_field_ow.append(row[field_index])

            # Realización de la carga de información en la segunda tabla
            with connection.cursor() as cursor_ow:                   
                # Contrucción del query para el volcado de datos            
                sql_statement = 'INSERT INTO db_backup_data_source_{0} ({1}) SELECT {1} FROM db_data_source_{0}'.format(self.input_name_table, fin_fields)
                # Ejecución del comando statenment
                cursor_ow.execute(sql_statement)
                # Se confirma la acción y se ejecuta en la base de datos                                                
                self.db_conn.commit()
            
            for index in range (len(list_field_ow)):
                try:
                    with connection.cursor() as cursor_ow:  
                        # Contrucción del query para el borrado de información particular                                                   
                        sql_statement = 'DELETE FROM {} WHERE {}=\'{}\' '.format(self.name_table, self.overwrite_field, list_field_ow[index])
                        # Ejecución del comando statenment                                        
                        cursor_ow.execute(sql_statement)
                        # Se confirma la acción y se ejecuta en la base de datos                            
                        self.db_conn.commit()

                except Exception as e:
                    self.db_conn.rollback()
                    raise ContentFileError

            # Obtencón de nuevo contenido, evitando duplicidad de información
            new_content = []
            list_index = []
            for row in range(len(content)-1, -1, -1):
                if not content[row] in new_content and not content[row][field_index] in list_index:
                    new_content.append(content[row])
                    list_index.append(content[row][field_index])

            content = new_content

        # Volcado en las tablas de tipo management
        if self.validate_empty_table() != 0 or current_reg:
            try:
                # Recolección de los campos existentes en la BD
                with connection.cursor() as cursor_ow:
                    man_field = [] 
                    fin_man_field = ''
                    # Campos provenientes de la tabla en base de datos
                    if self.db_engine == 'django.db.backends.mysql':
                        cursor_ow.execute('SHOW COLUMNS FROM db_management_{}'.format(self.input_name_table))

                    elif self.db_engine == 'sql_server.pyodbc':
                        cursor_ow.execute('SELECT name FROM sys.columns WHERE object_id = OBJECT_ID(\'db_management_{}\')'.format(self.name_table))

                    # Recorrido de información traida por la consulta, funciona SQL Server - MySQL
                    for field in cursor_ow.fetchall():
                        if not field[0] in self.not_file_fields:
                            # Se añade la posición con el nombre del campo con lower
                            man_field.append((field[0]).lower())

                # String con los campos finales (sin not_file_fields) del management
                fin_man_field = str(man_field).replace('[', '').replace(']', '').replace('\'', '')
                
                # Realización de la carga de información en la segunda tabla del management
                with connection.cursor() as cursor_ow:
                    # Contrucción del query para el volcado de datos            
                    sql_statement = 'INSERT INTO db_backup_management_{0} ({1}) SELECT {1} FROM db_management_{0}'.format(self.input_name_table, fin_man_field)
                    # Ejecución del comando statenment
                    cursor_ow.execute(sql_statement)
                    # Se confirma la acción y se ejecuta en la base de datos                                                
                    self.db_conn.commit()

                # Realización del eliminado de información de la primer tabla
                with connection.cursor() as cursor_ow:       
                    # Contrucción del query para la el borrado de datos                                    
                    sql_statement = 'DELETE FROM db_management_{}'.format(self.input_name_table)                    
                    # Ejecución del comando statenment                    
                    cursor_ow.execute(sql_statement)
                    # Se confirma la acción y se ejecuta en la base de datos                            
                    self.db_conn.commit()

            except Exception as e:
                self.db_conn.rollback()
                raise ContentFileError

        # CARGA DE INFORMACION 
    
        # Carga de información de acuerdo al tipo seleccionado (por registro)
        if self.fill_per_reg:
            # lista con los registros que no pudieron ser insertados
            wrong_reg = []    
            for register in content:
                # Contrucción del query para la inserción de datos
                new_register = (str(register)).replace('None', 'NULL')
                sql_statement = 'INSERT INTO {} {} VALUES {}'.format(self.name_table, name_fields, new_register)
                
                try:
                    # Ejecución del comando statenment
                    cursor.execute(sql_statement)
                    # Se confirma la acción y se ejecuta en la base de datos                
                    self.db_conn.commit()
                except Exception:
                    wrong_reg.append(register)
                    self.db_conn.rollback()

        # Carga de información de acuerdo al tipo seleccionado (por bloque)
        elif not self.fill_per_reg:        
            # Contrucción del query para la inserción de datos
            sql_statement = 'INSERT INTO {} {} VALUES ({})'.format(self.name_table, name_fields, char_accum)

            try:
                # Ejecución del comando statenment
                cursor.executemany(sql_statement, content)
                # Se confirma la acción y se ejecuta en la base de datos
                self.db_conn.commit()
            except Exception:
                self.db_conn.rollback()
                raise ContentFileError
                
        cursor.close()

    def validate_empty_table(self):
        '''
        Método encargado de validar el número de registros existentes
        en una tabla en particular de la BD.
        
        Retorna un valor entero el cual corresponde al número de 
        registros actuales de la tabla cosultada.
        '''
        # Preparación del objeto cursor usando el método cursor()        
        cursor = self.db_conn.cursor()
        # Contrucción del query para la inserción de datos
        sql_statement = 'SELECT COUNT(*) FROM {}'.format(self.name_table)
        # Ejecución del comando statenment        
        cursor.execute(sql_statement)
        num_reg = cursor.fetchone()[0]
        return num_reg

    def validate_table_existence(self):
        '''
        Método encargado de evitar la carga de datos en tablas
        inexistentes en la base de datos.
        
        Retorna un valor Booleano para el cual True supone que la tabla
        en la cual se pretende realizar la inserción existe previamente
        en la base de datos.
        '''
        # Lista con los nombres de las tablas en la base de datos
        database_tables = connection.introspection.table_names()
        # Validación de la existencia de la tabla
        for existing_tables in database_tables:
            if self.name_table == existing_tables:
                return True
        raise UnexsistanceError   
        return False

    def validate_parity_fields(self):
        '''
        Método encargado de evitar la carga de datos si se percibe
        disparidad entre los campos de la base de datos y los headers
        del archivo.
        
        Retorna un valor Booleano para el cual True supone que los
        campos en la base de datos son los mismos que los headers del
        archivo.
        '''
        # Preparación del objeto cursor usando el método cursor()        
        cursor = self.db_conn.cursor()
        # Campos provenientes del archivo Excel
        file_fields = self.charge_data()['fields']
        db_fields = []

        # Campos provenientes de la tabla en base de datos
        if self.db_engine == 'django.db.backends.mysql':
            cursor.execute('SHOW COLUMNS FROM {}'.format(self.name_table))

        elif self.db_engine == 'sql_server.pyodbc':
            cursor.execute('SELECT name FROM sys.columns WHERE object_id = OBJECT_ID(\'{}\')'.format(self.name_table))
        
        # Recorrido de información traida por la consulta, funciona SQL Server - MySQL
        for field in cursor.fetchall():
            if not field[0] in self.not_file_fields:
                # Se añade la posición con el nombre del campo con lower
                db_fields.append((field[0]).lower())
        cursor.close()

        # Comparación de las listas ordenadas para validar paridad
        if sorted(file_fields) == sorted(db_fields):
            return True
        else:
            raise ParityError
            return False

    def validate_empty_file(self):
        '''
        Método encargado de evitar la carga de datos si este carece de
        contenido, aún asi contenga headers.
        
        Retorna un valor Booleano para el cual True supone que el
        archivo posee contenido.
        '''
        if not self.charge_data()['content']:
            raise EmptyFileError
            return False
        else:
            return True

    def validate_user_file(self):
        '''
        Método encargado de evitar la carga de datos si este no posee
        el contenido correcto para cargar usuarios.
        
        Retorna un valor Booleano para el cual True supone que el
        archivo posee contenido válido para carga de usuarios.
        '''
        user_content = self.charge_data()['content']
        for users in user_content:
            for user in users:
                # Validación de la unicidad de columnas del archivo al verificar
                # que sea un string y no una lista
                if not isinstance(user, str):
                    raise ContentFileError
                    return False
        return True
    
    def validate_characters(self):
        '''
        Método encargado de evitar la carga de datos si este posee
        caracteres no permitidos.
        
        Retorna un valor Booleano para el cual True supone que el
        archivo posee contenido correcto.
        '''
        wrong_char = []

        file_fields = self.charge_data()['fields']
        file_content = self.charge_data()['content']

        # Si sólo existe una columna en el archivo
        if self.num_fields == 1:
            for data in file_content:
                # Búsqueda de datos que contengan el patrón de caracteres permitidos
                if re.search(self.allowed_char_pattern, str(data)):
                    wrong_char.append(data)
                                
        # Si el archivo consta de varias columnas
        else:
            # Recorrido del contenido
            for row in file_content:
                new_data_row = []
                for data in row:
                    # Búsqueda de datos que contengan el patrón de caracteres permitidos
                    if re.search(self.allowed_char_pattern, str(data)):
                        wrong_char.append(data)

        if wrong_char:
            raise ContentFileError
            return False
        else:
            return True                

    def charge_data(self):
        '''
        Método encargado de la ejecución del método fill_db luego
        de la realización de las validaciones.
        
        Retorna diccionario con los headers y el contenido del archivo
        con los cuales se realizará la inserción en la base de datos.
        '''
        # Ejecución de un método diferente de acuerdo a la extensión del archivo
        if self.file_name.lower().endswith(('.xlsx', '.xlsm', '.xlst', '.xltm')):
            data_content = self.charge_xlsx()['content']
            data_fields = self.charge_xlsx()['fields']

        elif self.file_name.lower().endswith(('.csv')):
            data_content = self.charge_csv()['content']
            data_fields = self.charge_csv()['fields']

        elif self.file_name.lower().endswith(('.xls')):
            data_content = self.charge_xls()['content']
            data_fields = self.charge_xls()['fields']
        else:
            raise FileFormatError
        
        return {'fields':data_fields, 'content':data_content}

    def execute(self):
        '''
        Método encargado de la ejecución del método fill_db luego
        de la realización de las validaciones.
        
        Retorna la ejecución del método fill_db.
        '''
        if self.validate_empty_file():
            if self.validate_characters():
                if self.charge_target == 'CAMPAIGN' or self.charge_target == 'MODEL':
                        if self.validate_table_existence():
                            if self.validate_parity_fields():
                                return self.fill_db(content=self.charge_data()['content'], fields=self.charge_data()['fields'])
                elif self.charge_target == 'USER':
                    if self.validate_user_file():
                        return self.fill_user(user_content=self.charge_data()['content'])

    def fill_user(self,user_content):
        '''
        Método encargado de realizar la carga masiva de usuarios
        en la base de datos (User y User_data).

        Parámetros:
        user_content -- Lista con los nombres de usuario de LDAP a cargar.
        '''
        # Listas para usuarios con conflicto para ser registrados
        unexistance_user = []
        registered_user = []

        for data_user in user_content:
            data = {}
            # Cuando el archivo contiene usuario y grupo, el dato se almacena como tupla así que esta condición va dirigida a la tupla
            # Mientras que cuento nada mas vine el usuario, el dato viene como str así que entra por el else
            if type(data_user) == tuple:
                try:
                    # Consulta de información proveniente de PYPERSONAL
                    ws_reponse = requests.post(
                                        settings.PYPERSONAL_URL,
                                        {
                                            'user': data_user[0],
                                            'fields': '["first_name", "last_name", "mail", "document"]'
                                        }
                                    )

                    content = ws_reponse.json()

                    if ws_reponse.status_code == requests.codes['ok']:
                        data['username'] = data_user[0]
                        data['first_name'] = content['epersonal']['first_name']
                        data['last_name'] = content['epersonal']['last_name']
                        data['email'] = content['ldap']['mail']

                    user_data = {
                        'document': content['ldap']['document'],
                        'login_type': 'LDAP'
                    }
                    try:
                        # Creación de registro en User, User_data y grupos
                        groups = []
                        group = Group.objects.get(name=data_user[1])
                        groups.append(group.id)

                        user = User.objects.create_user(**data)
                        UserData.objects.create(user=user, **user_data)
                        user.groups.set(groups)
                    except Exception:
                        registered_user.append(data_user)

                except Exception:
                    unexistance_user.append(data_user)

            else:
                try:
                    # Consulta de información proveniente de PYPERSONAL
                    ws_reponse = requests.post(
                                        settings.PYPERSONAL_URL,
                                        {
                                            'user': data_user,
                                            'fields': '["first_name", "last_name", "mail", "document"]'
                                        }
                                    )

                    content = ws_reponse.json()

                    if ws_reponse.status_code == requests.codes['ok']:
                        data['username'] = data_user
                        data['first_name'] = content['epersonal']['first_name']
                        data['last_name'] = content['epersonal']['last_name']
                        data['email'] = content['ldap']['mail']

                    user_data = {
                        'document': content['ldap']['document'],
                        'login_type': 'LDAP'
                    }
                    
                    # Validación de usuario registrado previamente
                    try:
                        user = User.objects.create_user(**data)
                        UserData.objects.create(user=user, **user_data)
                    except Exception:
                        registered_user.append(data_user)

                except Exception:
                    unexistance_user.append(data_user)
                