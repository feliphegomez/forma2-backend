'''
    tree by Juan David González Bedoya
'''


class Tree():
  def __init__(self, data, key_id = 'id', key_parent = 'parent'):
    self.data = data
    self.tree = []
    self.ids = []
    self.key_id = key_id
    self.key_parent = key_parent
  
  def generate(self, current_node = None):
    for node in self.data:
      if node[self.key_id] not in self.ids:
        node['children'] = []
        node['toggle'] = True
        if node[self.key_parent] is None and current_node is None:
          self.ids.append(node[self.key_id])
          self.tree.append(node)
          self.generate(node)
        elif current_node is not None:
          if node[self.key_parent] == current_node[self.key_id]:
            self.ids.append(node[self.key_id])
            current_node['children'].append(node)
            self.generate(current_node['children'][-1])
        
    return self.tree