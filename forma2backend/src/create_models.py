'''
Vista para la creación dinámica de modelos
'''
import re

from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.core.management import call_command
from django.db import models, connection

from .exceptions import (CharacterError, NoFieldError, NoModelNameError,
                         DuplicateError)


class GenerateModel:
    '''
    Clase para la validación de los modelos a crear
    '''

    def __init__(self, input_json, label, description):
        '''
        Método contructor encargado de inicializar atributos y ejecutar
        el método execute.

        Parámetros:
        input_json -- JSON ingresado
        label -- label del modelo a crear
        description -- descripción en español usada para las excepciones
        '''

        self.input_json = input_json
        self.jsonname = self.input_json['model']
        self.jsonfields = self.input_json['fields']
        self.model_label = label
        self.app_label = 'db'
        self.description = description
        self.execute()

    def define_model(self, name, fields):
        '''
        Método encargado de la definición de los modelos a crear. 

        Parámetros:
        name -- nombre del modelo a ser creado
        fields -- diccionario de campos tendrá el modelo

        Retorna el modelo a crear.
        '''

        class Meta:
            pass
        # Nombre arbitrario del módulo para ser usado como fuente del
        # modelo
        module = ''
        # Etiqueta de aplicación personalizada para el modelo
        app_label = self.app_label
        # Establecimiento de app_label utilizando la clase Meta
        setattr(Meta, 'app_label', app_label)
        # Establecimiento de un diccionario para la simulación de las
        # declaraciones dentro de una clase
        attrs = {
            '__module__': module,
            'Meta': Meta,
        }
        # Adición de los campos del modelo en el diccionario attrs
        attrs.update(fields)
        # Definción del modelo mediante el uso de type()
        model = type(name, (models.Model,), attrs)

        return model

    def change_fields(self):
        '''
        Método encargado de adaptar los campos que contendrá el modelo.

        Retorna un diccionario con los campos del modelo usando la 
        convención de nombres lower_case_with_underscores.
        '''

        # Diccionario creado para el ingreso de elementos
        dict_fields = {}
        for fields_content in self.jsonfields:
            type_field = fields_content['type']
            name_field = fields_content['name']
            type_field = type_field.lower()
            name_field = name_field.lower()
            have_obligatory = False
            # Cambio de espacios por underscore en los campos
            if re.search('[\s]', name_field):
                name_field = name_field.replace(' ', '_')
            # Recorrido de los campos en busca de la clave obligatory
            for name_fields_content in fields_content:
                if name_fields_content == 'obligatory':
                    have_obligatory = True
                    if fields_content['obligatory'] == 'True':
                        obligatory_fields = False
                    else:
                        obligatory_fields = True
            # Si no se encuentra la clave obligatory, obligatory_fields
            # tendrá por defecto el valor True
            if not have_obligatory:
                obligatory_fields = True
            # Adaptación del tipo de campo
            if type_field == 'string':
                dict_fields[name_field] = models.CharField(max_length=255, blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'int':
                dict_fields[name_field] = models.IntegerField(max_length=11, blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'date':
                dict_fields[name_field] = models.DateField(blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'list':
                dict_fields[name_field] = models.CharField(choices=self.change_list(), max_length=255, blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'identity':
                dict_fields[name_field] = models.IntegerField(max_length=11, blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'debt':
                dict_fields[name_field] = models.IntegerField(max_length=11, blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'product':
                dict_fields[name_field] = models.CharField(max_length=255, blank=obligatory_fields, null=obligatory_fields)
            elif type_field == 'dateauto':
                dict_fields[name_field] = models.DateField(blank=obligatory_fields, null=obligatory_fields, auto_now=True)

        return dict_fields

    def change_name(self):
        '''
        Método encargado de adaptar el nombre que tendrá el modelo.

        Retorna un valor tipo cadena con el nombre del modelo usando la
        convención de nombres lower_case_with_underscores.
        '''

        self.jsonname = self.jsonname.lower()
        # Cambio de espacios por underscore en el nombre del modelo
        if re.search('[\s]', self.jsonname):
            self.jsonname = self.jsonname.replace(' ', '_')

        self.jsonname = self.model_label + '_' + self.jsonname
        return self.jsonname

    def change_list(self):
        '''
        Método encargado de adaptar las opciones de los campos de tipo
        list para su posterior uso en el método change_fields.

        Retorna una tupla con el contenido de las opciones. 
        '''
        # Lista creada para permitir el ingreso de elementos,
        # convertida en tupla al finalizar
        tuple_options = []
        # Índice para permitir el ingreso de elementos en la lista
        list_index = 0
        # Recorrido de los campos en busca de la clave options
        for fields_content in self.jsonfields:
            for name_fields_content in fields_content:
                if name_fields_content == 'options':
                    options_keys = fields_content['options']
                    for options_content in options_keys:
                        # Ingreso de elementos a la lista
                        tuple_options.insert(list_index, ((options_content['value']), (options_content['field']),))
                        list_index += 1

        return (tuple(tuple_options))

    def validate_content_json(self):
        '''
        Método encargado de evitar la creación de modelos con deficit
        de contenido.

        Retorna un valor Booleano para el cual True supone que el 
        modelo a crear posee valores para su nombre, campos y opciones
        en el caso de un campo tipo list y False implica que alguno de
        estos se encuentra vacío.
        '''
        # Validación de contenido en el nombre del modelo
        if not self.jsonname:
            raise NoModelNameError
            return False
        # Validación de contenido en los campos del modelo
        for fields_content in self.jsonfields:
            # Verificación del nombre del campo
            if not fields_content['name']:
                raise NoFieldError
                return False
            # Verificación del tipo del campo
            if not fields_content['type']:
                raise NoFieldError
                return False
            # Verificación de las opciones del campo en caso de haberlas
            for name_fields_content in fields_content:
                if name_fields_content == 'options':
                    options_keys = fields_content['options']
                    for options_content in options_keys:
                        # Verificación del nombre de campo de las opciones
                        if not options_content['field']:
                            raise NoFieldError
                            return False
                        # Verificación del nombre de campo de las opciones
                        if not options_content['value']:
                            raise NoFieldError
                            return False

        return True

    def validate_characters_json(self):
        '''
        Método encargado de evitar la creación de modelos que contengan
        caracteres especiales.

        Retorna un valor Booleano para el cual True supone que el 
        modelo a crear carece de caracteres especiales y False implica
        que si los contiene.
        '''

        # Validación de caracteres especiales en el nombre del modelo
        if re.search('[^ a-zA-Z0-9 \_\-\–]', self.jsonname):
            raise CharacterError('el nombre de la campaña')
            return False
        # Validación de caracteres especiales en los campos del modelo
        for fields_content in self.jsonfields:
            # Verificación del nombre del campo
            if re.search('[^ a-zA-Z0-9 \_\-\–]', fields_content['name']):
                raise CharacterError(' en los campos de ' + self.description)
                return False
            # Verificación del tipo de campo
            if re.search('[^ a-zA-Z0-9]', fields_content['type']):
                raise CharacterError('')
                return False
            # Verificación de las opciones del campo en caso de haberlas
            for name_fields_content in fields_content:
                if name_fields_content == 'options':
                    options_keys = fields_content['options']
                    for options_content in options_keys:
                        # Verificación del nombre de campo de las opciones
                        if re.search('[^ a-zA-Z0-9 \_\-\–]', options_content['field']):
                            raise CharacterError(' en el campo opción de ' + self.description)
                            return False
                        # Verificación del nombre de campo de las opciones
                        if re.search('[^ a-zA-Z0-9 \_\-\–]', options_content['value']):
                            raise CharacterError('')
                            return False

        return True

    def validate_duplicate(self):
        '''
        Método encargado de evitar la creación de modelos duplicados.

        Retorna un valor Booleano para el cual True supone que el
        modelo a crear no existe y False implica que este ha sido 
        creado previamente. 
        '''

        # Lista con los nombres de las tablas en la base de datos
        database_tables = connection.introspection.table_names()
        # Variable con la etiqueta de la aplicación concatenado al
        # nombre del modelo a crear
        model_name = self.app_label + '_' + self.model_name
        # Validación de la existencia del modelo
        for existing_models in database_tables:
            if model_name == existing_models:
                raise DuplicateError()
                return False

        return True

    def execute(self):
        '''
        Método encargado de la ejecución del método define_model luego
        de verificar la validez del modelo.

        Retorna la ejecución del método define_model.
        '''

        # Si la validación de contenido arroja True el execute continúa
        if self.validate_content_json():
            # Si la validación de caracteres arroja True el execute
            # continúa
            if self.validate_characters_json():
                # Si la validación de duplicado arroja True el execute
                # continúa
                self.model_name = self.change_name()
                if self.validate_duplicate():
                    # Si no se presentan problemas en las validaciones
                    # se retorna un boleano con el evento de validación
                    # completo
                    return True

    def create_model(self):
        '''
        Método encargado de la creación de los modelos en la base de
        datos
        '''
        # Usando SchemaEditor es creado el modelo definido previamente
        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(self.define_model(self.model_name, self.change_fields()))
